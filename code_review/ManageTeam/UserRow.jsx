import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Axios from 'axios';
import { Button, Form, FormGroup, FormControl } from 'react-bootstrap';

import Avatar from '../Elements/Avatar.jsx';
import ReassignPanel from './ReassignPanel.jsx';

// set token when request is made
// check them to cancel existing request if new one is made or component is unmounted
let CancelToken = Axios.CancelToken;
let cancelUpdate,
		cancelRemove;

class UserRow extends Component {
	constructor(props) {
		super(props);

		this.state = {
			permissionLevel: this.props.user.permission,
			accessMessage: this.props.user.access_message,
			successDisplay: false,
			isPanelOpen: false,
			eligibleUsers: this.props.eligibleUsers,
		}
	}

	componentWillReceiveProps(nextProps) {
		if (this.state.eligibleUsers !== nextProps.eligibleUsers) {
			this.setState({
				eligibleUsers: nextProps.eligibleUsers,
			});
		}
	}

	componentWillUnmount() {
		// check for cancel token to see if a request is running
		// cleanup and cancel existing requests
		if (cancelUpdate != undefined) {
		  cancelUpdate();
		}

		if (cancelRemove != undefined) {
		  cancelRemove();
		}

		// check for timeout running and cleanup any existing
		if (this.timeout) {
		  clearTimeout(this.timeout);
		}
	}

	onClickRemove = (event) => {
		event.preventDefault();

		if (this.props.user.reassign_message) {
			this.toggleReassignPanel();
		} else {
			this.removeUser();
		}
	}

	removeUser = (event) => {
		if (event) {
			event.preventDefault()
		};

		var token = "null";
		if (document != undefined && document.querySelector('meta[name=csrf-token]') != null) {
			token = document.querySelector('meta[name=csrf-token]').getAttribute('content');
		}

		Axios({
		  method: 'post',
		  url: `/access/revoke_access`,
		  data: {
		  	id: this.props.user.id,
		  	object_id: this.props.objectId,
		  	object_type: this.props.objectType.toLowerCase()
		  },
		  headers: {
		    'X-CSRF-Token': token,
				'X-Requested-With': 'XMLHttpRequest',
		  },
		  cancelToken: new CancelToken(function executor(c) {
		    cancelRemove = c;
		  })
		}).then(() => {
			// run callback to update users and remove this from display
			if (this.props.updateUsersCallback) {
				this.props.updateUsersCallback();
			}

			cancelRemove = undefined;
		}).catch((err) => {
			cancelRemove = undefined;
		  console.log(err);
		});
	}

	toggleReassignPanel = () => {
		let isOpen = this.state.isPanelOpen;
		this.setState({
			isPanelOpen: !isOpen,
		});
	}

	handlePermissionChange = (event) => {
		event.preventDefault();

		const target = event.target;
		let token = document.querySelector('meta[name=csrf-token]').getAttribute('content');

		this.setState({
			permissionLevel: event.target.value,
		});

		Axios({
		  method: 'post',
		  url: '/access/update_access.json',
		  data: {
		  	email: this.props.user.email,
		  	permission: target.value,
		  	object_id: this.props.objectId,
		  	object_type: this.props.objectType.toLowerCase()
		  },
		  headers: {
		    'X-CSRF-Token': token,
				'X-Requested-With': 'XMLHttpRequest',
		  },
		  cancelToken: new CancelToken(function executor(c) {
		    cancelUpdate = c;
		  })
		}).then((response) => {
			this.setState({
				accessMessage: response.data.access_message,
				successDisplay: true,
			});

			this.timeout = window.setTimeout(() => {
			  this.setState({
			    successDisplay: false,
			  });

			  this.timeout = null;
			}, 5000);

			cancelUpdate = undefined;
		}).catch((err) => {
			cancelUpdate = undefined;

		  console.log(err);
		});
	}

	updateOwner = (owner, message) => {
		this.props.updateCallback(owner, message);
	}

	getUserName = (user) => {
		if (user) {
			if (user.full_name) {
				return(
					<div>
					<h4 className="user-title">{`${user.full_name}`}</h4>
					<p className="text-muted">{user.email}</p>
					</div>
				);
			} else if (user.short_name) {
				return(
					<div>
					<h4 className="user-title">{`${user.short_name}`}</h4>
					<p className="text-muted">{user.email}</p>
					</div>
				);
			} else {
				return <h4 className="user-title">{`${this.state.type} member`}</h4>;
			}
		} else {
			return <h4 className="user-title">{`${this.state.type} member`}</h4>;
		}
	}

	getPermission = () => {
  	const currentRole = this.props.currentUserRole,
  				userRole = this.props.user.role;

  	if (this.props.canAdmin) {
  		return true;
  	} else if (this.props.currentUserPermission === 'edit') {
  		// users without canAdmin level must have edit permission to reassign

  		if (currentRole === userRole) {
  			// if the current user is same role level as target user, can edit regardless of level
  			return true;
  		} else if (currentRole === 'admin') {
  			// admins can always edit
  			return true;
  		} else if ((currentRole === 'attorney' || currentRole === 'paralegal') && (userRole === 'user' || userRole === 'hr')) {
  			// attorneys and paralegals have the same permission level and can edit basic users
  			return true;
  		} else if (currentRole === 'hr' && userRole === 'user') {
  			// hrs can edit basic users
  			return true;
  		} else {
  			// if the current user is a basic user and the target user is higher than that, can't edit
  			return false;
  		}
  	} else {
  		// if user doesn't have have canAdmin or edit permission, can't edit
  		return false;
  	}

	}

	// I'd rename it to renderDropdown since it returns JSX.
	getDropdown = () => {
		if (this.props.isOwner) {	
			// if user is the object owner, only option is to reassign
			// reassign only available to those with higher permission + edit access

			return(
				<span className="access-label">
					<i className="fa fa-star-o" />
					<h4>Owner</h4>

					{this.props.currentUserPermission === "edit" && this.getPermission() ?
						<Button
							bsStyle="link"
							onClick={this.toggleReassignPanel}
							id={`remove-user-${this.props.user.id}`}
							className="remove-link pull-right"
						>
							Reassign {this.props.objectType}
						</Button>
					: null }
				</span>
			);
		} else if (this.props.currentUserPermission === "edit" && this.getPermission()) {
			// current_users with edit access can edit permissions and remove any user of equal or higher role status
			return(
				<div className="access-update-form">
					<Form inline>
						<FormGroup id="updateAccessLevel">
				      <FormControl
				      	componentClass="select"
				      	placeholder="select"
				      	onChange={this.handlePermissionChange}
				      	value={this.state.permissionLevel}
				      >
				      	<option
				      		className="access_level"
				      		value="edit"
				      	>
				      		Can edit
				      	</option>

				      	<option
				      		className="access_level"
				      		value="view"
				      	>
				      		Can view
				      	</option>
				      </FormControl>
				    </FormGroup>
					</Form>

					<Button
						bsStyle="link"
						onClick={this.onClickRemove}
						id={`remove-user-${this.props.user.id}`}
						className="remove-link pull-right"
					>
						<i className="fa fa-times" />Remove
					</Button>
				</div>
			);
		} else {
			// current_users with view-only access or role lower than this user can only see their info
			return(
				<span className="access-label">
					<i className="fa fa-lock" />
					<h4>{`Can ${this.state.permissionLevel}`}</h4>
				</span>
			);
		}
	}

	render() {
		let accessMessage;
		if (this.state.accessMessage) {
			accessMessage = this.state.accessMessage.charAt(0).toUpperCase() + this.state.accessMessage.slice(1);
		}

		return(
			<div className="team-member">
				<div className="row">
					<div className="col-xs-2 col-md-1">
						<Avatar
							user={this.props.user}
							thumbnail={this.props.user.thumbnail}
						/>
					</div>

					<div className="col-xs-5 col-md-6">
						{this.getUserName(this.props.user)}
					</div>

					<div className="col-xs-5 col-md-4 col-md-offset-1">
						<div className="row">
							<div className="col-xs-12">
								{this.getDropdown()}
							</div>
						</div>

						<div className="row">
							<div className="col-xs-12 access-message">
								<p className="text-muted"><em>{this.props.objectType.toLowerCase() !== "task" ? accessMessage : null}</em></p>
								{this.state.successDisplay ?
									<span className="access-success text-success"><i className="fa fa-check fa-lg" /> Access updated</span>
								: null }
							</div>
						</div>
					</div>
				</div>

				{this.state.isPanelOpen ?
					<div className="row">
						<div className="col-xs-12 access-message">
							<div className="reassign-panel">
							  <ReassignPanel
							    objectId={this.props.objectId}
							    matterSlug={this.props.matterSlug}
							    objectType={this.props.objectType.toLowerCase()}
							    isPanelOpen={this.state.isPanelOpen}
							    toggleReassignPanel={this.toggleReassignPanel}
							    eligibleUsers={this.state.eligibleUsers}
							    ownerId={this.props.user.id}
							    updateCallback={this.updateOwner}
							    reassignMessage={this.props.user.reassign_message}
							    userId={this.props.user.id}
							  />
							</div>
						</div>
					</div>
				: null }

				<hr />
			</div>
		);
	}

}

UserRow.propTypes = {
	updateCallback: PropTypes.func.isRequired,
	user: PropTypes.object.isRequired,
	objectId: PropTypes.number.isRequired,
	updateUsersCallback: PropTypes.func,
	matterSlug: PropTypes.string,
	currentUserRole: PropTypes.string,
	currentUserPermission: PropTypes.string,
	objectType: PropTypes.string,
	isOwner: PropTypes.bool,
	eligibleUsers: PropTypes.array,
};

UserRow.defaultProps = {
	updateUsersCallback: null,
	matterSlug: '',
	currentUserRole: 'user',
	currentUserPermission: 'view',
	objectType: 'task',
	isOwner: false,
	eligibleUsers: [],
};

export default UserRow;
