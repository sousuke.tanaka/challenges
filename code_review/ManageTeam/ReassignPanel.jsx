import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Axios from 'axios';
import { Panel, Form, FormControl, FormGroup, ControlLabel, HelpBlock, Button, Modal } from 'react-bootstrap';

import InputSelect from '../Elements/InputSelect.jsx';

// set token when request is made
// check them to cancel existing request if new one is made or component is unmounted
let CancelToken = Axios.CancelToken;
let cancel;

class ReassignPanel extends Component {
	constructor(props) {
		super(props);

		this.state = {
			newOwnerEmail: '',
			isPanelOpen: this.props.isPanelOpen,
			eligibleUsers: this.props.eligibleUsers,
			ownerId: this.props.ownerId,
			suggestions: []
		}
	}

	componentWillReceiveProps(nextProps) {
		if (this.state.isPanelOpen !== nextProps.isPanelOpen) {
			this.setState({
				isPanelOpen: nextProps.isPanelOpen,
			});
		}

		if (this.state.eligibleUsers !== nextProps.eligibleUsers || this.state.ownerId !== nextProps.ownerId) {
			this.setState({
				eligibleUsers: nextProps.eligibleUsers,
				ownerId: nextProps.ownerId,
			});
		}
	}

	componentWillUnmount() {
	  // check for cancel token to see if a request is running
	  // cleanup and cancel existing requests
	  if (cancel != undefined) {
	    cancel();
	  }
	}

	onSuggestionsClearRequested = () => {
		this.setState({
			suggestions: [],
		});
	}

	onSuggestionsFetchRequested = ({ value }) => {
		// don't get suggestions until at least 3 characters
		if (value.length > 2) {

			Axios.get(`/access/reassign_autocomplete.json?object_id=${this.props.matterSlug}&object_type=Matter&search=${value}`)
				.then((response) => {
					let newData = response.data;
					this.setState({ suggestions: newData.users });
				}).catch(function(err) {
					console.log(err);
				});
		}
	}

	handleChange = (e) => {
		const target = e.target;

    this.setState({
      newOwnerEmail: target.value,
    });
	}

	handleSubmit = (e) => {
		e.preventDefault();

		// I'd rather define a token selector helper function.
		let token = document.querySelector('meta[name=csrf-token]').getAttribute('content');

		let userObj = {};

		if (this.props.reassignMessage && this.props.objectType.toLowerCase() !== "task") {
			// if we're both reassigning and removing a user, run remove callback
			Axios({
			  method: 'post',
			  url: `/access/revoke_access`,
			  data: {
			  	id: this.props.userId,
			  	object_id: this.props.objectId,
			  	object_type: this.props.objectType.toLowerCase(),
			  	reassign_email: this.state.newOwnerEmail
			  },
			  headers: {
			    'X-CSRF-Token': token,
					'X-Requested-With': 'XMLHttpRequest',
			  },
			  cancelToken: new CancelToken(function executor(c) {
			    cancel = c;
			  })
			}).then((response) => {
				// run callback to update users and remove this from display
				userObj = {
					userId: response.data[0].id,
					userName: this.getUserName(response.data[0], false),
					user: response.data[0]
				}
				this.props.updateCallback(userObj);
				cancel = undefined;
			}).catch((err) => {
				cancel = undefined;
			  console.log(err);
				alert("Unable to reassign, please make sure this user has already been invited to Bridge.");
			});
		} else {
			Axios({
			  method: 'patch',
			  url: `/matters/${this.props.matterSlug}/change_owner_for`,
			  data: {
			  	object_type: this.props.objectType.toLowerCase(),
			  	object_id: this.props.objectId,
			  	user_email: this.state.newOwnerEmail,
			  },
			  headers: {
			    'X-CSRF-Token': token,
					'X-Requested-With': 'XMLHttpRequest',
			  },
			  cancelToken: new CancelToken(function executor(c) {
			    cancel = c;
			  })
			}).then((response) => {
				this.clearForm();
				// callback to update users in team modal/matter row
				userObj = {
					userId: response.data[0].id,
					userName: this.getUserName(response.data[0], false),
					user: response.data[0]
				}
				this.props.updateCallback(userObj, `${this.props.objectType} reassigned`);
				cancel = undefined;
			}).catch((err) => {
				alert("Unable to reassign, please make sure this user has already been invited to Bridge.");
				cancel = undefined;
			  console.log(err);
			});
		}
	}

	// Method used for the InputSelect to get current owner to be added
	// As a rule of thumb, it's common practice to use prefix handle rather than on for event handlers.
	onChangeListener = (val) => {
		this.setState({newOwnerEmail: val});
	}

	clearForm = () => {
		this.setState({
			newOwnerEmail: '',
			isPanelOpen: false,
		});

		this.props.toggleReassignPanel();
	}

	removeOwner = () => {
		let users = [];
		for(let i = 0; i < this.state.eligibleUsers.length; i++)
		{
			if(this.state.eligibleUsers[i].id != this.state.ownerId)
			{
				users.push(this.state.eligibleUsers[i]);
			}
		}
		return users;
	}

	getUserName = (user, get_full_name) => {
		if (user) {
			if (get_full_name && user.full_name) {
				return `${user.full_name}`;
			} else if (user.short_name) {
				return user.short_name;
			} else if (user.email) {
				return user.email;
			} else {
				return `${this.props.objectType} member`;
			}
		} else {
			return `${this.props.objectType} member`;
		}
	}

	// I'd use renderForm rather than getForm as it returns JSX.
	getForm = () => (
		<Form
			onSubmit={this.handleSubmit}
			inline
			className="pull-right"
		>
      <FormGroup controlId="reassignOwner" className="form-select-group">
				<ControlLabel>{this.props.reassignMessage ? this.props.reassignMessage : "Reassign to:"}</ControlLabel>
				<InputSelect
					swithToInputText="Someone else"
					dropdownPlaceholder="Select new owner..."
					inputPlaceholder="Search for users..."
					options={this.removeOwner(this.state.eligibleUsers)}
					uniqueKey="id"
					value="email"
					name="full_name"
					onChangeListener={this.onChangeListener}
					suggestions={this.state.suggestions}
					suggestionKey="email"
					onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
					onSuggestionsClearRequested={this.onSuggestionsClearRequested}
				/>
      </FormGroup>

    	<FormGroup className="form-btn-group text-right">
      	{this.props.reassignMessage ?
      		<HelpBlock className="text-left">Reassign {this.props.objectType.toLowerCase()} and delete this user?</HelpBlock>
    		: null }
    		<Button
    			type="submit"
    			className="btn-auto"
					id="reassign"
    			disabled={this.state.newOwnerEmail ? false : true}
    		>
    		  {this.props.reassignMessage ? "Confirm" : "Reassign"}
    		</Button>
        <Button
	        type="reset"
	        onClick={this.clearForm}
        	bsStyle="link"
        	className="btn-link-padded"
        >
        	Cancel
      	</Button>
    	</FormGroup>
		</Form>
	)

	render() {
		

		return(
			<div>
				{this.props.isModal ?
					<Modal
						show={this.state.isPanelOpen}
						onHide={this.clearForm}
						backdropClassName="dark-modal-backdrop"
						dialogClassName="bridge-modal reassign-panel"
					>
						<Modal.Body>
							{this.getForm()}
						</Modal.Body>
					</Modal>
				:
					<Panel
						collapsible
						expanded={this.state.isPanelOpen}
						className={this.state.isPanelOpen ? "open-panel" : "hidden-panel"}
					>
						{this.getForm()}
					</Panel>
				}
			</div>
		);
	}

}

ReassignPanel.propTypes = {
	toggleReassignPanel: PropTypes.func.isRequired,
	objectId: PropTypes.number.isRequired,
	matterSlug: PropTypes.string,
	ownerId: PropTypes.number,
	eligibleUsers: PropTypes.array,
	objectType: PropTypes.string,
	isPanelOpen: PropTypes.bool,
	reassignMessage: PropTypes.string,
	updateCallback: PropTypes.func,
};

ReassignPanel.defaultProps = {
	matterSlug: null,
	ownerId: null,
	eligibleUsers: [],
	objectType: 'task',
	isPanelOpen: false,
	reassignMessage: null,
	updateCallback: null,
};

export default ReassignPanel;
