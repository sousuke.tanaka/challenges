@map = {}

def match?(pattern, str)
  return true if pattern.empty? && str.empty?
  return false if pattern.empty? || str.empty?

  key = pattern[0]
  pat = @map[key]

  if pat
    return false if !str.start_with?(pat)
    return match? pattern[1..-1], str[pat.length..-1]
  end

  (1..str.length).each do |i|
    @map[key] = str[0..i-1]
    
    is_matching = match? pattern[1..-1], str[i..-1]
    return true if is_matching

    @map.delete(key)
  end

  return false
end